# Dockerfile
FROM php:8.1-fpm

WORKDIR /var/www/html

# Install system dependencies
RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip \
    git

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install PHP extensions
RUN docker-php-ext-install zip pdo_mysql

# Copy project files
COPY . .

# Install project dependencies
RUN composer install --optimize-autoloader --no-dev

# Run Laravel specific commands during container initialization
RUN php artisan key:generate --env=docker
RUN php artisan migrate --env=docker
RUN php artisan db:seed --env=docker

# Command to start the application
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=8000", "--env=docker"]
