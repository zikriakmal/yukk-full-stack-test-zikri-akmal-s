# HOW TO RUN THIS PROJECT IN LOCAL

### PREREQUISITES: 
- php ^8.0
- composer ^2.0

### STEPS 
- clone this project 
- setup env and set db name and password 
- composer install 
- npm install
- php artisan serve
- php artisan key:generate
- php artisan migrate
- php artisan db:seed

### MANUAL TEST CREDENTIAL 
in seed file there are user already seeds
- email : user@gmail.com
- password : password


