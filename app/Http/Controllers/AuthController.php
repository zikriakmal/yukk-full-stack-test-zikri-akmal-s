<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function loginView(): \Illuminate\Contracts\Foundation\Application|Factory|View|Application|RedirectResponse
    {
        if (Auth::check()) {
            // Redirect authenticated users to the home page or any other page
            return redirect()->route('/');
        }

        // If not authenticated, show the login page
        return view('auth.login');
    }

    public function registerView():Illuminate\Contracts\Foundation\Application|Factory|View|Application|RedirectResponse
    {
        if (Auth::check()) {
            // Redirect authenticated users to the home page or any other page
            return redirect()->route('/');
        }
        return view('auth.register');
    }

    public function loginProcess(Request $request): RedirectResponse
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }

    public function registerProcess(Request $request): RedirectResponse
    {
        $credentials = $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required'],
        ]);

        $user = User::create([
            'name' => $credentials['name'],
            'email' => $credentials['email'],
            'password' => Hash::make($credentials['password']),
            'email_verified_at' => Date::now(),
        ]);

        Wallet::create([
            'user_id' => $user['id'],
            'balance' => 0,
        ]);

        Auth::login($user);

        return redirect()->intended('/')
            ->with(['welcome_message' => 'User created, Your balance is 0, let\'s top up']);
    }

    public function logoutProcess(Request $request): RedirectResponse
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

}
