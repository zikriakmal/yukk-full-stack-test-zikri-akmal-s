<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Models\User;
use App\Models\Wallet;
use App\Models\WalletJournal;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{

    public function dashboard()
    {
        return view('dashboard.index');
    }

    public function getWalletJournals(Request $request)
    {
        $walletJournals = WalletJournal::select([
            'wallet_journals.id',
            'wallet_journals.transaction_id',
            'wallet_journals.transaction_type_id',
            'wallet_journals.transaction_proof_path',
            'wallet_journals.amount',
            'wallet_journals.description',
            'wallet_journals.created_at',
            'transaction_types.name as transaction_type_name'
        ])
            ->leftJoin('transaction_types', 'wallet_journals.transaction_type_id', '=', 'transaction_types.id')
            ->where('user_id', Auth::id());

        return DataTables::of($walletJournals)
            ->addColumn('action', function ($walletJournal) {
                $imagePath = $walletJournal->transaction_proof_path;
                if (!$imagePath) {
                    return '<button type="button" class="btn btn-light" disabled>Image not available</button>';
                }

                return '<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#imageModal' . $walletJournal->id . '" style="color: #1a202c">
        show image
                    </button>
                    <div class="modal fade" id="imageModal' . $walletJournal->id . '" tabindex="-1" style="z-index: 999" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <img src="' . config('APP_URL') . '/storage/' . $imagePath . '" alt="Transaction Proof" class="img-fluid">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary text-black" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>';
            })
            ->rawColumns(['action'])
            ->make(true);;
    }

    public function topUpView(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('dashboard.top_up');
    }

    public function paymentView(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('dashboard.payment');

    }

    public function topUpProcess(Request $request): \Illuminate\Http\RedirectResponse
    {
        $transaction = $request->validate([
            'amount' => ['required'],
            'description' => ['required'],
            'image' => ['required', 'image', 'max:2048'], // Adjust the max file size as needed
        ]);

        try {
            DB::beginTransaction();

            // Save the image to storage
            $imagePath = $request->file('image')->store('top_up_images', 'public');

            // Rest of your existing code
            $walletUser = Wallet::where('user_id', Auth::id())->first();
            $walletUser->increment('balance', $transaction['amount']);
            $walletUser->save();

            $walletJournal = new WalletJournal();
            $walletJournal->user_id = Auth::id();
            $walletJournal->amount = $transaction['amount'];
            $walletJournal->transaction_id = 'TRX_TU_' . time() . '_' . Auth::id();
            $walletJournal->transaction_type_id = 1;
            $walletJournal->description = $transaction['description'];
            $walletJournal->transaction_proof_path = $imagePath; // Save the image path in the database
            $walletJournal->save();

            DB::commit();

        } catch (\Throwable $e) {
            dd($e);
            DB::rollback();
            return back()->withErrors(['server-error' => 'Internal server error, please try again later']);
        }
        return redirect()->back();
    }

    public function paymentProcess(Request $request): Application|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $transaction = $request->validate([
            'amount' => ['required'],
            'description' => ['required'],
        ]);

        $walletUser = Wallet::where('user_id', Auth::id())->first();
        if ($transaction['amount'] > $walletUser->balance) {
            return back()->withErrors(['amount' => 'amount shouldn\'nt greater than balance']);
        }

        try {
            $walletUser->decrement('balance', $transaction['amount']);
            $walletUser->save();

            $walletJournal = new WalletJournal();
            $walletJournal->user_id = Auth::id();
            $walletJournal->amount = $transaction['amount'];
            $walletJournal->transaction_id = 'TRX_PYMNT_' . time() . '_' . Auth::id();
            $walletJournal->transaction_type_id = 2;
            $walletJournal->description = $transaction['description'];
            $walletJournal->save();
            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            return back()->withErrors(['server-error' => 'Internal server error, please try again letter']);
        }

        return redirect()->back();
    }


}
