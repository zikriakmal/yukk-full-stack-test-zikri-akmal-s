<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'user1',
            'email' => 'user@gmail.com',
            'password' => Hash::make('password'),
            'email_verified_at' => Date::now(),
            'created_at' => Date::now(),
            'updated_at' => Date::now()
        ]);
    }
}
