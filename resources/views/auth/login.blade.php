@extends('layouts.app')
@section('title','LOGIN')

@section('content')
    <div class="flex items-center justify-center h-screen w-screen">
        <div
            class="bg-white max-w-72 mx-4 p-[20px] m-4 flex-row flex-1 flex-column justify-items-center items-center border-2 rounded-xl">
            <h1 class="text-center">LOGIN</h1>
            <form method="post" action="{{ route('auth.login-process') }}">
                @csrf
                <div>
                    <label for="email" class="block text-sm font-medium leading-6 text-gray-900">Email address</label>
                    <div class="mt-2">
                        <input required id="email"
                               name="email"
                               type="email"
                               autocomplete="email"
                               value="{{ old('name') }}"
                               class="px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">

                    </div>
                </div>
                <div>
                    <label for="password" class="block text-sm font-medium leading-6 text-gray-900">
                        Password</label>
                    <div class="mt-2">
                        <input required id="password"
                               name="password"
                               type="password"
                               autocomplete="email"
                               class=" px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                    @error('email')
                    <p class="text-red-500 text-sm mt-1">{{ $message }}</p>
                    @enderror
                </div>
                <div style="margin:10px 0;">
                    <p>Need Account ? <a href="{{route('register')}}"> register here</a></p>
                    <div style="margin-top: 10px">
                        <button class="w-block" type="submit"
                                style="width: 100%;color: white; background-color: darkgreen; border-radius: 5px; padding:10px 10px; align-items: center; justify-content: center">
                            LOGIN
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
