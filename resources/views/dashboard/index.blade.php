@extends('layouts.app')
@section('title','LOGIN')
@section('sidebar')
    @section('content')
        <div class="container overflow-x-scroll rounded-2xl bg-white p-16">
            <h1 class="font-bold py-4">Transaction History</h1>
            <table class="table" id="usersDataTable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Transaction ID</th>
                    <th>Transaction Type</th>
                    <th>Amount</th>
                    <th>Description</th>
                    <th>Transaction Date</th>
                    <th>Uploaded Top up</th>
                </tr>
                </thead>
                <tbody>
                <!-- Your table body content here -->
                </tbody>
            </table>
        </div>

        @push('scripts')
            <script>
                $(document).ready(function () {
                    $('#usersDataTable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: "{{ route('get-wallet-journals') }}",
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'transaction_id', name: 'transaction_id'},
                            {data: 'transaction_type_name', name: 'transaction_type_id'},
                            {
                                data: 'amount',
                                name: 'amount',
                                render: function (data, type, row) {
                                    // Format the amount as currency
                                    return parseFloat(data).toLocaleString('id-ID', {
                                        style: 'currency',
                                        currency: 'IDR'
                                    });
                                }
                            },
                            {data: 'description', name: 'description'},
                            {
                                data: 'created_at',
                                name: 'created_at',
                                render: function (data, type, row) {
                                    return moment(data).format('DD-MM-YYYY HH:mm:ss');
                                }
                            },
                            {data: 'action', name: 'action', orderable: false, searchable: false},
                        ],
                        order: [[5, 'desc']]
                    });

                    // Event listener for modal hidden event
                    $('#imageModal').on('hidden.bs.modal', function () {
                        $('.modal-backdrop').remove();
                    });
                });
            </script>
        @endpush
    @endsection
