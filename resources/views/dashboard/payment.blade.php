@extends('layouts.app')
@section('title','TOP UP')
@section('content')
    <div class="max-w-96 mx-auto border-0 shadow-2xl p-16 rounded-b-md">
        <h1 class="text-center">PAYMENT</h1>
        <form method="post" action="{{ route('user.payment-process') }}">
            @csrf
            <div>
                <label for="amount" class="block text-sm font-medium leading-6 text-gray-900">Amount</label>
                <div class="mt-2">
                    <input required id="amount"
                           name="amount"
                           type="number"
                           autocomplete="amount"
                           value="{{ old('name') }}"
                           class="px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    @error('amount')
                        <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div>
                <label for="description" class="block text-sm font-medium leading-6 text-gray-900">
                    Description</label>
                <div class="mt-2">
                    <textarea required id="description"
                           name="description"
                           type="text"
                           autocomplete="amount"
                              class=" px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"></textarea>
                </div>

            </div>
            <div style="margin:10px 0;">
                <div style="margin-top: 10px">
                    <button class="w-block" type="submit"
                            style="width: 100%;color: white; background-color: darkgreen; border-radius: 5px; padding:10px 10px; align-items: center; justify-content: center">
                       PAYMENT
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
