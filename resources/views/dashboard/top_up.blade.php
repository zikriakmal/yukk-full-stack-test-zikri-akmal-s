@extends('layouts.app')
@section('title','TOP UP')
@section('content')
    <div class="max-w-96 mx-auto border-0 shadow-2xl p-16  rounded-b-md overscroll-y-auto"
         style="height: 95vh; overflow-y: auto;">
        <h1 class="text-center">TOP UP</h1>
        <form method="post" action="{{ route('user.top-up-process') }}" enctype="multipart/form-data">
            @csrf
            <div>
                <label for="amount" class="block text-sm font-medium leading-6 text-gray-900">Amount</label>
                <div class="mt-2">
                    <input required id="amount"
                           name="amount"
                           type="number"
                           autocomplete="amount"
                           value="{{ old('amount') }}"
                           class="px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">

                </div>
            </div>
            <div>
                <label for="description" class="block text-sm font-medium leading-6 text-gray-900">
                    Description</label>
                <div class="mt-2">
                    <textarea required id="description"
                              name="description"
                              type="text"
                              autocomplete="description"
                              class=" px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"></textarea>
                </div>
            </div>
            <div class="mb-4">
                <label for="document" class="block text-sm font-medium text-gray-600">Choose a Image</label>
                <input required onchange="previewImage()" type="file" name="image" id="image"
                       class="mt-1 p-2 border rounded-md w-full">
            </div>
            <div class="flex-1 mb-4">
                <label class="block text-sm font-medium text-gray-600">Preview</label>
                <div>
                    <img
                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAKlBMVEX////p6Oj19fXy8fHu7u7r6er5+fn8/Pzv7+/r6+vn5ubz8/P6+fr9/P0SPnzDAAACrUlEQVR4nO3c4ZaTMBBA4QKVlFbf/3V1dXu2ZGZCspLOwLnf37K412ASoMfLBQAAAAAAAAAAAAAAAAAAAAAAAAAAACXzGELHwusQwukLf1BIIYXuKKSQQn8UUlhbeLukN/rpUvhO5y+cHQqXjn+OROFeKOyHwr1Q+D/SeFuW65j0T09QOD3P+1A/PnxhWl5OrA3j0QvTsKIccfTC7J7lKo84eOE8ZObiIQcsnPLCSRxy8MIlL5TnPnZhygOHQUynFO6lU6G8SoMXNr/BFIVyuQhVON9bH3CMeaH8OwpVuDQ/w/mVF8pDIhWO+iAUZYOo/HSkQvOXLFmt+XK9D1X4HI7vJ6o/GacwlX9RW7ot94+zTvotcJzCx9cBcvtcltI8J+MWP07hanvSmlgSpvA2dEqMUpjf6FnXXLsoheL94m6JQQrFvbr+VOnysdluvIKDFIodtJn4aP1HGqNQDqFxMf+dcu9NC2aMQmUIB/XB2ed5tN2ZJUShuAcyE2fzE1OEQnmn/iTGarHOYYtQ+LJf20h8GWxrrhUCFIq7WDNx/eCpckoNULjer5USs+e/dVOqf6HyRNDoEGtKVaJ/YXkIh5erUX5zrGbVcC9UF3t1qLQjK1YN98Kar/T9G0V1TdmeUr0LK4bwM9FaU7YSvQvNxT7PsCekjVXDudDar8lE8abwS3lKdS6sDSzTv4QRorB6CDeU3gX4Fu4U+GfVsOcb18LClruVvWp4Fm7t19pYU6pn4Y5DWEj0LCwsAN+irxpnKtRXjVMVqqvGuQq3HjafoDDYt016FA7iof/pCsWqccLC+3pKdS1cOlktjN53wP1RuBcK+6FwLxT241L41v84aXIo9EMhhRT6o5BCCv1RSGHJOEVQfFkMAAAAAAAAAAAAAAAAAAAAAAAAAACw6Tc89C+gXlz0SwAAAABJRU5ErkJggg=="
                        id="image-preview"
                        class="mt-1 mx-auto border rounded-md w-42 h-42 bg-cover"
                        alt="Image Preview"
                    >
                </div>
            </div>

            <div style="margin:10px 0;">
                <div style="margin-top: 10px">
                    <button class="w-block" type="submit"
                            style="width: 100%;color: white; background-color: darkgreen; border-radius: 5px; padding:10px 10px; align-items: center; justify-content: center">
                        TOP UP
                    </button>
                </div>
            </div>
        </form>
        <script>
            function previewImage() {
                const input = document.getElementById('image');
                const preview = document.getElementById('image-preview');

                if (input.files && input.files[0]) {
                    const reader = new FileReader();

                    reader.onload = function (e) {
                        preview.src = e.target.result;
                    };

                    reader.readAsDataURL(input.files[0]);
                } else {
                    preview.src = '';
                }
            }
        </script>
    </div>
@endsection
