<!-- resources/views/layouts/app.blade.php -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Yukk - @yield('title')</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css">
    <style>
        .modal-backdrop {
            z-index: -1;
        }
    </style>

</head>

<body class="font-sans bg-gray-100">
<div class="flex h-screen bg-opacity-70 backdrop-blur">
    @if(!in_array(Route::currentRouteName(), ['login', 'register']))
        <button id="toggleButton"
                class="p-2.5 text-gray-500 focus:outline-none md:hidden bg-white rounded-xl shadow-2xl"
                style="position: absolute;left: 15px;top:15px;z-index: 999;">
            <svg class="h-6 w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                 xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                      d="M4 6h16M4 12h16m-7 6h7"></path>
            </svg>
        </button>
    @endif
    @section('sidebar')
        <!-- Sidebar -->
        @if(!in_array(Route::currentRouteName(), ['login', 'register']))
            <div id="sidebar"
                 style="z-index: 99;   backdrop-filter: blur(10px); background-color: rgba(255, 255, 255, 0.9);"
                 class="bg-gray-200  text-gray-700 w-64 p-4 fixed h-screen overflow-y-auto transition-transform duration-300 transform -translate-x-full md:relative md:transform md:translate-x-0 backdrop-blur">
                <div class="flex items-center mb-8 pt-[50px]">
                    <img
                        src="https://media.licdn.com/dms/image/D5603AQFGuvVarDrbeg/profile-displayphoto-shrink_400_400/0/1668306382622?e=2147483647&v=beta&t=jkwOH214yjPrbZaIn4JvLbMlL7W8e9C_I4DFnXDoL2M"
                        alt="Avatar" class="w-[75px] h-[75px] shadow-2xl p-2 rounded-full mr-4">
                    <div>
                        <h2 class="text-lg font-semibold">{{ auth()->user()->name }}</h2>
                        <p class="text-sm text-gray-500">Wallet Balance: </p>
                        <p class="text-sm text-gray-500 font-semibold">
                            Rp.{{  number_format(auth()->user()->wallet->balance, 0, ',', '.')}}</p>
                    </div>
                </div>
                <ul>
                    <li>
                        <a href="{{ route('/') }}"
                           style="text-decoration: none"
                           class="block py-2 px-4 rounded hover:bg-gray-300 {{Route::currentRouteName() === '/' ? 'bg-gray-300' : '' }} ">TRANSACTION
                            HISTORY</a>
                    </li>
                    <li>
                        <a href="{{ route('top-up') }}"
                           style="text-decoration: none"
                           class="block py-2 px-4 rounded hover:bg-gray-300 {{Route::currentRouteName() === 'top-up' ? 'bg-gray-300' : '' }} ">TOP
                            UP</a>
                    </li>
                    <li>
                        <a href="{{ route('payment') }}"
                           style="text-decoration: none"
                           class="block py-2 px-4 rounded hover:bg-gray-300  {{Route::currentRouteName() === 'payment' ? 'bg-gray-300' : '' }}">PAYMENT</a>
                    </li>
                    <li style="margin-top:20px;">
                        <form method="post" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit"
                                    class="block py-2 px-4 rounded bg-red-400 hover:bg-red-500 text-white">
                                LOGOUT
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        @endif
    @show

    <!-- Content -->
    <div class="py-[50px] flex-1 flex flex-col overflow-hidden bg-opacity-30 backdrop-blur p-2">
        <div class="flex-1">
            @yield('content')
        </div>
    </div>


    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script>
        const toggleButton = document.getElementById('toggleButton');
        const sidebar = document.getElementById('sidebar');
        toggleButton.addEventListener('click', () => {
            sidebar.classList.toggle('-translate-x-full');
        });
    </script>
    @stack('scripts')
</div>
</body>

</html>
