<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// GUARDED | AUTHENTICATED
Route::middleware(['auth', 'auth.session'])->group(function () {
    Route::get('/', [UserController::class, 'dashboard'])->name('/');
    Route::get('get-wallet-journals', [UserController::class, 'getWalletJournals'])->name('get-wallet-journals');
    Route::post('logout', [AuthController::class, 'logoutProcess'])->name('logout');

    Route::controller(UserController::class)->prefix('user')->group(function () {
        Route::get('payment', 'paymentView')->name('payment');
        Route::get('top-up', 'topUpView')->name('top-up');

        Route::post('top-up-process', 'topUpProcess')->name('user.top-up-process');
        Route::post('payment-process', 'paymentProcess')->name('user.payment-process');
    });

});

Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::get('login', 'loginView')->name('login');
    Route::get('register', 'registerView')->name('register');

    Route::post('login-process', 'loginProcess')->name('auth.login-process');
    Route::post('register-process', 'registerProcess')->name('auth.register-process');
});
